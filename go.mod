module gitlab.com/tobidot/gitla

go 1.22

require (
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/mmcloughlin/profile v0.1.1
	golang.org/x/term v0.19.0
)

require golang.org/x/sys v0.19.0 // indirect
