SELECT 
  author_date,
  parent,
  repo,
  subject
FROM
  commits
WHERE
  author_name = {Your Git Name}
ORDER BY
  author_date DESC
