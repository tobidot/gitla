SELECT parent, repo, COUNT()
FROM commits
GROUP BY parent, repo
ORDER BY COUNT() DESC
LIMIT 10;
