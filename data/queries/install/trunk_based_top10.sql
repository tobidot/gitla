SELECT 
  repo, 
  AVG(is_on_main) 
FROM commits 
GROUP BY repo 
ORDER BY AVG(is_on_main) DESC 
LIMIT 10;