package domain

import "errors"

var ErrMissingConfig = errors.New("the config file is missing")
