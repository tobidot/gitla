package domain

import "fmt"

type CompletionInput struct {
	CommandToBeCompleted string
	PartialWord          string
	FullLine             string
	PartialLine          string
	PrecedingLine        string
}

type Config struct {
	Root      string
	DBPath    string
	LogParser LogParserConfig
}

type LogParserConfig struct {
	LogParserDelimiterPrefix string
}

type Entries struct {
	Entries []Entry
}

func (entries Entries) Len() int {
	return len(entries.Entries)
}

func (entries Entries) Less(i, j int) bool {
	return entries.Entries[i].Committer.Date < entries.Entries[j].Committer.Date
}

func (entries Entries) Swap(i, j int) {
	entries.Entries[i], entries.Entries[j] = entries.Entries[j], entries.Entries[i]
}

type Entry struct {
	CommitHash   string
	ParentHashes string
	FullPath     string
	Parent       string
	Repo         string
	Committer    Person
	Author       Person
	Subject      string
	Body         string
	Diff         string
}

type Table struct {
	Columns []string
	Rows    [][]string
}

type LastCommit struct {
	CommitHash string
	CommitDate string
}

func (entry *Entry) ToString() string {
	return fmt.Sprintf(`%s
└─ %s
└─ %s
└─ %s
└─── %s
%s

`,
		entry.FullPath,
		entry.CommitHash,
		entry.Author.Date,
		entry.Author.Email,
		entry.Subject,
		entry.Body,
	)
}

type Person struct {
	Name  string
	Email string
	Date  string
}

type SubCommand string

const (
	Fetch                SubCommand = "fetch"
	Log                  SubCommand = "log"
	Search               SubCommand = "search"
	CreateOrUpdateConfig SubCommand = "createOrUpdateConfig"
	Query                SubCommand = "query"
)

var SubCommands = []SubCommand{Fetch, Log, Search, CreateOrUpdateConfig, Query}
