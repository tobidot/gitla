package domain

import "flag"

type CommitsRepository interface {
	Open(string) error
	Close() error
	CreateCommitsTableIfNotExists() error
	CreateFullTextSearchTableIfNotExists() error
	CreateFullTextSearchTriggerIfNotExists() error
	DumpCommitsToCommitsTable([]Entry)
	MarkCommitsAsOnMain([]string, string)
	GetLastCommitDateForPath(string) []LastCommit
	QueryFTS(string) (Entries, error)
	QueryFile(string) (Table, error)
}

type Shell interface {
	RunCommandInPath(path string, command string) (string, string, error)
}

type ConfigRepository interface {
	ReadConfig(configDirPath string, defaultConfig Config) (Config, error)
	WriteConfig(configDirPath string, config Config) error
}

type Completion interface {
	Complete(args []string, flags *flag.FlagSet)
}
