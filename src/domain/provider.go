package domain

import (
	"math/rand"
	"time"
)

func ProvideRandomEntries() Entries {
	return Entries{
		Entries: []Entry{ProvideRandomEntry(), ProvideRandomEntry(), ProvideRandomEntry()},
	}
}

func ProvideRandomEntry() Entry {
	commonPrefix := UnsafeRandomString("Entry", 3) + "-"
	return Entry{
		CommitHash:   commonPrefix + UnsafeRandomString("commitHash", 3),
		ParentHashes: commonPrefix + UnsafeRandomString("parentHash1", 3) + " " + commonPrefix + UnsafeRandomString("parentHash2", 3),
		FullPath:     commonPrefix + UnsafeRandomString("fullPath", 3),
		Committer:    provideRandomPerson(commonPrefix),
		Author:       provideRandomPerson(commonPrefix),
		Subject:      commonPrefix + UnsafeRandomString("subject", 6),
		Body:         commonPrefix + UnsafeRandomString("body", 100),
		Diff:         commonPrefix + UnsafeRandomString("diff", 20),
	}
}

func provideRandomPerson(prefix string) Person {
	commonPrefix := UnsafeRandomString("Person", 3) + "-"

	return Person{
		Name:  prefix + commonPrefix + UnsafeRandomString("name", 3),
		Email: prefix + commonPrefix + UnsafeRandomString("email", 3),
		Date:  UnsafeRandomDate().Format(time.RFC3339),
	}
}

func UnsafeRandomDate() time.Time {
	minimum := time.Date(1970, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	maximum := time.Date(2070, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := maximum - minimum

	// #nosec G404 -- Use of weak random number generator (math/rand instead of crypto/rand)
	// Only used for tesdata
	sec := rand.Int63n(delta) + minimum
	return time.Unix(sec, 0)
}

func UnsafeRandomString(prefix string, n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		// #nosec G404 -- Use of weak random number generator (math/rand instead of crypto/rand)
		// Only used for tesdata
		s[i] = letters[rand.Intn(len(letters))]
	}
	return prefix + "-" + string(s)
}
