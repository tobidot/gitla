package subcommands

import (
	"fmt"
	"log"
	"path/filepath"

	"gitlab.com/tobidot/gitla/src/adapter/jsonconfig"
	"gitlab.com/tobidot/gitla/src/adapter/shell"
	"gitlab.com/tobidot/gitla/src/adapter/sqlite"
	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/service/branchfinder"
	"gitlab.com/tobidot/gitla/src/service/config"
	"gitlab.com/tobidot/gitla/src/service/fetch"
	"gitlab.com/tobidot/gitla/src/service/find"
	"gitlab.com/tobidot/gitla/src/service/formatter"
	"gitlab.com/tobidot/gitla/src/service/gitlog"
)

type CoreService struct {
	Database domain.CommitsRepository
}

func CreateOrUpdateConfig(jconf *jsonconfig.JSONConfig, existingConfig domain.Config) (err error) {
	err = config.MakeConfigDirIfNotExists()
	if err != nil {
		return err
	}
	gitlaConfigDir := config.GetGitlaConfigDirPath()
	if existingConfig == (domain.Config{}) {
		existingConfig = config.NewDefaultConfig(gitlaConfigDir)
	}

	configFilePath := filepath.Join(gitlaConfigDir, jsonconfig.ConfigFileName)
	err = jconf.WriteConfig(gitlaConfigDir, existingConfig)
	if err != nil {
		return fmt.Errorf("error while writing config: %v", err)
	}
	fmt.Printf("Config file updated at %s\n", configFilePath)
	return nil
}

func Fetch(root string) (err error) {
	gitDirs, err := find.GetAllGitRepositories(root)
	if err != nil {
		return fmt.Errorf("error while getting repositories: %v", err)
	}

	nGitDirs := len(gitDirs)
	bash := shell.Bash{}
	for i, dir := range gitDirs {
		err := fetch.Fetch(&bash, dir)
		if err != nil {
			fmt.Printf("%d/%d error while fetching from %s\n", i, nGitDirs, dir)
		} else {
			fmt.Printf("%d/%d git fetch `%s`\n", i, nGitDirs, dir)
		}
	}
	log.Printf("🎉 Successfully called `git fetch` on %d repositories", len(gitDirs))
	return nil
}

func LogToDB(root string, dbPath string, logParserConfig domain.LogParserConfig) (err error) {
	gitDirs, err := find.GetAllGitRepositories(root)
	if err != nil {
		return fmt.Errorf("error while getting repositories: %v", err)
	}

	service := CoreService{Database: &sqlite.Database{}}
	err = service.Database.Open(dbPath)
	if err != nil {
		return fmt.Errorf("error opening database: %v", err)
	}
	defer func() {
		err = service.Database.Close()
	}()

	err = service.Database.CreateCommitsTableIfNotExists()
	if err != nil {
		return fmt.Errorf("error creating commits table: %v", err)
	}

	err = service.Database.CreateFullTextSearchTableIfNotExists()
	if err != nil {
		return fmt.Errorf("creating full text search tables: %v", err)
	}

	err = service.Database.CreateFullTextSearchTriggerIfNotExists()
	if err != nil {
		return fmt.Errorf("creating full text search trigger: %v", err)
	}

	var commitsCounter int
	logParser := gitlog.LogParserNew(logParserConfig)
	bash := shell.Bash{}

	for i, gitDir := range gitDirs {
		lastCommits := service.Database.GetLastCommitDateForPath(gitDir)
		commits := gitlog.GetAllNewCommits(&bash, &logParser, lastCommits, gitDir)
		fmt.Printf("%d/%d %s: Adding %d new commits\n", i+1, len(gitDirs), gitDir, len(commits))
		service.Database.DumpCommitsToCommitsTable(commits)
		mainHash, err := gitlog.GetMainHeadCommitHash(&bash, &logParser, gitDir)
		if err == nil {
			mainHashParents := branchfinder.GetParents(mainHash, commits)
			// Update the is_on_main flag for all commits in the main branch
			service.Database.MarkCommitsAsOnMain(mainHashParents, gitDir)
		}

		commitsCounter += len(commits)
	}

	fmt.Printf("%d new commits added to %s\n", commitsCounter, dbPath)
	return nil
}

func Search(dbPath string, term string) (err error) {
	service := CoreService{Database: &sqlite.Database{}}
	err = service.Database.Open(dbPath)
	if err != nil {
		return err
	}
	defer func() {
		err = service.Database.Close()
	}()

	entries, err := service.Database.QueryFTS(term)
	if err != nil {
		return err
	}

	for _, entry := range entries.Entries {
		fmt.Print(entry.ToString())
	}
	return nil
}

func Query(dbPath string, queryName string) (err error) {
	service := CoreService{Database: &sqlite.Database{}}
	err = service.Database.Open(dbPath)
	if err != nil {
		return err
	}
	defer func() {
		err = service.Database.Close()
	}()
	queryFilePath := filepath.Join(config.GetGitlaQueryDirPath(), queryName)
	table, err := service.Database.QueryFile(queryFilePath)

	formatter.PrintPagedTable(table)
	return nil
}
