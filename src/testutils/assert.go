package testutils

import (
	"errors"
	"strings"
	"testing"
)

func AssertAllEq[K comparable](t *testing.T, a []K, b []K) {
	t.Helper()

	AssertEq(t, len(a), len(b))
	for i := range a {
		AssertEq(t, a[i], b[i])
	}
}

func AssertEq[K comparable](t *testing.T, a K, b K) {
	t.Helper()

	if a != b {
		t.Error(a, "not equal to", b)
	}
}

func AssertNotEq[K comparable](t *testing.T, a K, b K) {
	t.Helper()

	if a == b {
		t.Error(a, "not equal to", b)
	}
}

func AssertContainsBy[C, E comparable](t *testing.T, hay []C, needle E, by func(C) E) {
	t.Helper()

	found := false
	for _, element := range hay {
		if by(element) == needle {
			found = true
		}
	}

	if !found {
		t.Error(hay, "does not contain", needle)
	}
}

func AssertStringContains(t *testing.T, hay string, needle string) {
	t.Helper()

	if !strings.Contains(hay, needle) {
		t.Error(hay, "does not contain", needle)
	}
}

func AssertNil(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Error(err)
	}
}

func AssertErrorOfEqType(t *testing.T, err error, expected error) {
	t.Helper()

	if !errors.Is(err, expected) {
		t.Error(err, "is not the same as expected", expected)
	}
}

func AssertErrorNotOfType(t *testing.T, err error, expected error) {
	t.Helper()

	if errors.Is(err, expected) {
		t.Error(err, "is not the same as expected", expected)
	}
}
