package fetch

import (
	"log"

	"gitlab.com/tobidot/gitla/src/domain"
)

func Fetch(shell domain.Shell, path string) error {
	command := "git fetch"
	_, stderr, err := shell.RunCommandInPath(path, command)
	if err != nil {
		log.Printf("Command `%s` exited with error code %s in path %s: \n%s", command, err, path, stderr)
	}
	return err
}
