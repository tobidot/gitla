package branchfinder

import (
	"errors"
	"log"
	"strings"

	"gitlab.com/tobidot/gitla/src/domain"
)

var ErrCyclicalCommitAncestry = errors.New("cyclical map chain found")

func GetParents(startCommitHash string, commits []domain.Entry) []string {
	commitToFirstParentMap := make(map[string]string)
	for _, commit := range commits {
		if commit.ParentHashes != "" {
			commitToFirstParentMap[commit.CommitHash] = getFirstParent(commit.ParentHashes)
		}
	}
	result, err := collectMapChain(commitToFirstParentMap, startCommitHash)
	if err != nil {
		log.Fatalf("Error while trying to find all parents of origin/main HEAD: %v", err)
	}
	return result
}

func getFirstParent(parentHashes string) string {
	return strings.Split(parentHashes, " ")[0]
}

func collectMapChain(rawMap map[string]string, start string) ([]string, error) {
	result := make([]string, 0)
	seen := make(map[string]bool)
	currentCommit := start

	for {
		if seen[currentCommit] {
			return []string{}, ErrCyclicalCommitAncestry
		}

		result = append(result, currentCommit)
		seen[currentCommit] = true

		nextCommit, exists := rawMap[currentCommit]
		if !exists {
			break
		}
		currentCommit = nextCommit
	}

	return result, nil
}
