package branchfinder

import (
	"testing"

	"gitlab.com/tobidot/gitla/src/testutils"
)

func Test_getFirstParent(t *testing.T) {
	t.Run("single parent", func(t *testing.T) {
		parentHashes := "abc123"
		result := getFirstParent(parentHashes)
		testutils.AssertEq(t, result, "abc123")
	})

	t.Run("multiple parents", func(t *testing.T) {
		parentHashes := "abc123 def456 ghi789"
		result := getFirstParent(parentHashes)
		testutils.AssertEq(t, result, "abc123")
	})

	t.Run("empty string", func(t *testing.T) {
		parentHashes := ""
		result := getFirstParent(parentHashes)
		testutils.AssertEq(t, result, "")
	})
}

func Test_collectMapChain(t *testing.T) {
	t.Run("simple chain", func(t *testing.T) {
		rawMap := map[string]string{
			"commit1": "commit2",
			"commit2": "commit3",
		}
		result, err := collectMapChain(rawMap, "commit1")
		testutils.AssertNil(t, err)
		testutils.AssertAllEq(t, result, []string{"commit1", "commit2", "commit3"})
	})

	t.Run("single commit", func(t *testing.T) {
		rawMap := map[string]string{}
		result, err := collectMapChain(rawMap, "commit1")
		testutils.AssertNil(t, err)
		testutils.AssertAllEq(t, result, []string{"commit1"})
	})

	t.Run("cyclical chain", func(t *testing.T) {
		rawMap := map[string]string{
			"commit1": "commit2",
			"commit2": "commit3",
			"commit3": "commit1",
		}
		result, err := collectMapChain(rawMap, "commit1")
		testutils.AssertErrorOfEqType(t, err, ErrCyclicalCommitAncestry)
		testutils.AssertEq(t, len(result), 0)
	})

	t.Run("empty start commit", func(t *testing.T) {
		rawMap := map[string]string{
			"commit1": "commit2",
		}
		result, err := collectMapChain(rawMap, "")
		testutils.AssertNil(t, err)
		testutils.AssertAllEq(t, result, []string{""})
	})
}
