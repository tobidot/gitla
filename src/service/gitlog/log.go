package gitlog

import (
	"bytes"
	"fmt"
	"log"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"gitlab.com/tobidot/gitla/src/domain"
)

type LogParserDelimiterConfig struct {
	Prefix         string
	CommitHash     string
	ParentHashes   string
	Entry          string
	AuthorName     string
	AuthorEmail    string
	AuthorDate     string
	CommitterName  string
	CommitterEmail string
	CommitterDate  string
	Subject        string
	Body           string
	Diff           string
}

func newLogParserDelimiters(prefix string) LogParserDelimiterConfig {
	return LogParserDelimiterConfig{
		Prefix:         prefix,
		CommitHash:     "commit hash",
		ParentHashes:   "parent hashes",
		Entry:          "entry",
		AuthorName:     "author name",
		AuthorEmail:    "author email",
		AuthorDate:     "author Date",
		CommitterName:  "committer name",
		CommitterEmail: "committer email",
		CommitterDate:  "committer date",
		Subject:        "subject",
		Body:           "body",
		Diff:           "diff",
	}
}

type LogParser struct {
	compiledSectionRegex     *regexp.Regexp
	logFormat                string
	logParserDelimiterConfig LogParserDelimiterConfig
}

func LogParserNew(logParserConfig domain.LogParserConfig) LogParser {
	logParserDelimiters := newLogParserDelimiters(logParserConfig.LogParserDelimiterPrefix)
	return LogParser{
		compiledSectionRegex:     regexp.MustCompile("^" + logParserDelimiters.Prefix + " (.*)$"),
		logFormat:                getLogFormat(logParserDelimiters),
		logParserDelimiterConfig: logParserDelimiters,
	}
}

// GetAllNewCommits retrieves all new commits for lastCommitDate onwards (exclusive)
func GetAllNewCommits(shell domain.Shell, logParser *LogParser, lastCommits []domain.LastCommit, path string) []domain.Entry {
	entries, _ := logParser.GitLog(shell, lastCommits, path)
	for i := range entries.Entries {
		entries.Entries[i].FullPath = path
		entries.Entries[i].Parent = getRepoParentNameFromPath(path)
		entries.Entries[i].Repo = getRepoNameFromPath(path)
	}
	var result []domain.Entry
	for _, entry := range entries.Entries {
		isAlreadyInLastCommits := contains(lastCommits, entry)
		if !isAlreadyInLastCommits {
			result = append(result, entry)
		}
	}
	return result
}

func GetMainHeadCommitHash(shell domain.Shell, logParser *LogParser, path string) (string, error) {
	return logParser.GetMainHeadCommitHash(shell, path)
}

func contains(lastCommits []domain.LastCommit, entry domain.Entry) bool {
	isAlreadyInLastCommits := false
	for _, lastCommit := range lastCommits {
		if lastCommit.CommitHash == entry.CommitHash {
			isAlreadyInLastCommits = true
		}
	}
	return isAlreadyInLastCommits
}

func (logParser *LogParser) getRawLog(shell domain.Shell, lastCommits []domain.LastCommit, path string) (string, error) {
	var command string
	if len(lastCommits) > 0 {
		command = fmt.Sprintf("git log --all --after=\"%s\" --name-status --format=\"%s\"", lastCommits[0].CommitDate, logParser.logFormat)
	} else {
		command = fmt.Sprintf("git log --all --name-status --format=\"%s\"", logParser.logFormat)
	}
	stdout, stderr, err := shell.RunCommandInPath(path, command)
	if err != nil {
		if strings.HasPrefix(stderr, "fatal: your current branch") && strings.HasSuffix(stderr, "does not have any commits yet\n") {
			return "", nil
		}
		log.Printf("Command `git log` exited with error code %s in path %s with error: \n%s", err, path, stderr)
		return "", err
	}
	return stdout, err
}

func getLogFormat(logParserDelimiters LogParserDelimiterConfig) string {
	formatTemplate := `
{{ .Prefix }} {{ .Entry }}
{{ .Prefix }} {{ .CommitHash }}
%H
{{ .Prefix }} {{ .ParentHashes }}
%P
{{ .Prefix }} {{ .AuthorName }}
%an
{{ .Prefix }} {{ .AuthorEmail}}
%ae
{{ .Prefix }} {{ .AuthorDate }}
%aI
{{ .Prefix }} {{ .CommitterName }}
%cn
{{ .Prefix }} {{ .CommitterEmail }}
%ce
{{ .Prefix }} {{ .CommitterDate }}
%cI
{{ .Prefix }} {{ .Subject }}
%s
{{ .Prefix }} {{ .Body }}
%b
{{ .Prefix }} {{ .Diff }}
`
	temp, err := template.New("myTemplate").Parse(formatTemplate)
	if err != nil {
		log.Fatal(err)
	}

	var b bytes.Buffer
	err = temp.Execute(&b, logParserDelimiters)
	if err != nil {
		log.Fatal(err)
	}

	format := b.String()
	return format
}

func (logParser *LogParser) matchSectionLine(line string) (string, bool) {
	submatches := logParser.compiledSectionRegex.FindStringSubmatch(line)
	if len(submatches) == 2 {
		return submatches[1], true
	}
	return "", false
}

func (logParser *LogParser) writeToEntry(entry *domain.Entry, section string, content string) {
	switch section {
	case logParser.logParserDelimiterConfig.CommitHash:
		entry.CommitHash = content
	case logParser.logParserDelimiterConfig.ParentHashes:
		entry.ParentHashes = content
	case logParser.logParserDelimiterConfig.AuthorName:
		entry.Author.Name = content
	case logParser.logParserDelimiterConfig.AuthorDate:
		entry.Author.Date = content
	case logParser.logParserDelimiterConfig.AuthorEmail:
		entry.Author.Email = content
	case logParser.logParserDelimiterConfig.CommitterName:
		entry.Committer.Name = content
	case logParser.logParserDelimiterConfig.CommitterDate:
		entry.Committer.Date = content
	case logParser.logParserDelimiterConfig.CommitterEmail:
		entry.Committer.Email = content
	case logParser.logParserDelimiterConfig.Subject:
		entry.Subject = content
	case logParser.logParserDelimiterConfig.Body:
		entry.Body = content
	case logParser.logParserDelimiterConfig.Diff:
		entry.Diff = content
	}
}

func (logParser *LogParser) parseLog(raw string) domain.Entries {
	var entries domain.Entries
	lines := strings.Split(raw, "\n")

	var entry domain.Entry
	var currentSection string
	var currentContent string
	var section string
	var isSection bool
	for _, line := range lines {
		section, isSection = logParser.matchSectionLine(line)

		if isSection {
			logParser.writeToEntry(&entry, currentSection, strings.Trim(currentContent, "\n"))
			currentContent = ""
			currentSection = section

			if section == "entry" {
				if entry.Subject != "" {
					entries.Entries = append(entries.Entries, entry)
					entry = domain.Entry{}
				}
			}
		} else {
			currentContent += "\n" + line
		}
	}
	if entry.CommitHash != "" {
		logParser.writeToEntry(&entry, currentSection, strings.Trim(currentContent, "\n"))
		entries.Entries = append(entries.Entries, entry)
	}
	return entries
}

func (logParser *LogParser) GitLog(shell domain.Shell, lastCommits []domain.LastCommit, path string) (domain.Entries, error) {
	out, err := logParser.getRawLog(shell, lastCommits, path)
	if err != nil {
		return domain.Entries{}, err
	}
	entries := logParser.parseLog(out)
	return entries, err
}

func (logParser *LogParser) GetMainHeadCommitHash(shell domain.Shell, path string) (string, error) {
	var command = "git rev-parse origin/HEAD"
	stdout, stderr, err := shell.RunCommandInPath(path, command)
	if err != nil {
		if strings.HasPrefix(stderr, "fatal: ambiguous argument 'origin/HEAD': unknown revision or path not in the working tree.") {
			return "", nil
		}
		log.Printf("Command `git rev-parse` exited with error code %s in path %s with error: \n%s", err, path, stderr)
		return "", err
	}
	return strings.TrimSpace(stdout), err
}

func getRepoNameFromPath(path string) string {
	return filepath.Base(path)
}

func getRepoParentNameFromPath(path string) string {
	return filepath.Base(filepath.Dir(path))
}
