package gitlog

import (
	"testing"

	"gitlab.com/tobidot/gitla/src/adapter/shell"
	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/testutils"
)

func Test_gitLog(t *testing.T) {
	logParserConfig := domain.LogParserConfig{
		LogParserDelimiterPrefix: "€€€",
	}
	logParser := LogParserNew(logParserConfig)
	bash := shell.Bash{}

	t.Run("apply to this repo causes no error", func(t *testing.T) {
		entries, err := logParser.GitLog(&bash, []domain.LastCommit{}, ".")
		testutils.AssertNil(t, err)
		testutils.AssertNotEq(t, len(entries.Entries), 0)
	})
}

func Test_getRawLog(t *testing.T) {
	logParserConfig := domain.LogParserConfig{
		LogParserDelimiterPrefix: "€€€",
	}
	logParser := LogParserNew(logParserConfig)
	bash := shell.Bash{}

	t.Run("apply to this repo causes no error", func(t *testing.T) {
		output, err := logParser.getRawLog(&bash, []domain.LastCommit{}, ".")
		testutils.AssertNil(t, err)
		if len(output) == 0 {
			t.Error(output, "is empty.")
		}
	})
}

func Test_parseLog(t *testing.T) {
	logParserConfig := domain.LogParserConfig{
		LogParserDelimiterPrefix: "€€€",
	}
	logParser := LogParserNew(logParserConfig)

	t.Run("empty log creates no entries", func(t *testing.T) {
		log := ""
		entries := logParser.parseLog(log)
		if len(entries.Entries) != 0 {
			t.Logf("Expected zero entries for empty log, actual: %d", len(entries.Entries))
			t.Fail()
		}
	})

	t.Run("2 logs are parsed correctly", func(t *testing.T) {
		log := `
€€€ entry
€€€ commit hash
47383a3403494661f2f13a2b15a7709aec7e432c
€€€ parent hashes
9c9bfbf730fb6ab48176261853d981dff9cb35d1
€€€ author name
Tobias Hermann
€€€ author email
post@tobidot.eu
€€€ author Date
2022-10-12T08:43:33+02:00
€€€ committer name
Tobias Hermann
€€€ committer email
post@tobidot.eu
€€€ committer date
2022-10-12T08:43:33+02:00
€€€ subject
Clean up project structure
€€€ body

€€€ diff


M	main.go
A	src/adapter/git/common.go
A	src/adapter/git/fetch.go
A	src/adapter/git/log.go
A	src/adapter/sqlite/sqlite.go
R097	model.go	src/domain/model.go

€€€ entry
€€€ commit hash
9c9bfbf730fb6ab48176261853d981dff9cb35d1
€€€ parent hashes

€€€ author name
Tobias Hermann
€€€ author email
post@tobidot.eu
€€€ author Date
2022-10-12T08:22:20+02:00
€€€ committer name
Tobias Hermann
€€€ committer email
post@tobidot.eu
€€€ committer date
2022-10-12T08:22:20+02:00
€€€ subject
First rough draft
€€€ body

€€€ diff


A	.gitignore
A	go.mod
A	go.sum
A	main.go
A	model.go
`
		entries := logParser.parseLog(log)
		testutils.AssertEq(t, len(entries.Entries), 2)
		testutils.AssertEq(t, entries.Entries[0].CommitHash, "47383a3403494661f2f13a2b15a7709aec7e432c")
		testutils.AssertEq(t, entries.Entries[1].CommitHash, "9c9bfbf730fb6ab48176261853d981dff9cb35d1")
		testutils.AssertStringContains(t, entries.Entries[1].Diff, "A\tmodel.go")
	})
}

func Test_getLogTemplate(t *testing.T) {
	logParserConfig := domain.LogParserConfig{
		LogParserDelimiterPrefix: "€€€",
	}
	logParser := LogParserNew(logParserConfig)

	expectedTemplate := `
€€€ entry
€€€ commit hash
%H
€€€ parent hashes
%P
€€€ author name
%an
€€€ author email
%ae
€€€ author Date
%aI
€€€ committer name
%cn
€€€ committer email
%ce
€€€ committer date
%cI
€€€ subject
%s
€€€ body
%b
€€€ diff
`
	format := logParser.logFormat
	testutils.AssertEq(t, format, expectedTemplate)
}

func Test_getRepoNameFromPath(t *testing.T) {
	t.Run("empty Path results relative path", func(t *testing.T) {
		testutils.AssertEq(t, getRepoNameFromPath(""), ".")
	})

	t.Run("relative stays same", func(t *testing.T) {
		testutils.AssertEq(t, getRepoNameFromPath("dir"), "dir")
	})

	t.Run("absolute path selects correct base", func(t *testing.T) {
		testutils.AssertEq(t, getRepoNameFromPath("/bla/blub/dir"), "dir")
	})
	t.Run("relative path selects correct base", func(t *testing.T) {
		testutils.AssertEq(t, getRepoNameFromPath("./bla/blub/dir"), "dir")
	})
}

func Test_getRepoParentNameFromPath(t *testing.T) {
	t.Run("empty Path becomes dot", func(t *testing.T) {
		testutils.AssertEq(t, getRepoParentNameFromPath(""), ".")
	})

	t.Run("direct subfolder becomes dot", func(t *testing.T) {
		testutils.AssertEq(t, getRepoParentNameFromPath("dir"), ".")
	})

	t.Run("absolute path selects correct parent", func(t *testing.T) {
		testutils.AssertEq(t, getRepoParentNameFromPath("/bla/blub/dir"), "blub")
	})
	t.Run("relative path selects correct parent", func(t *testing.T) {
		testutils.AssertEq(t, getRepoParentNameFromPath("./bla/blub/dir"), "blub")
	})
}
