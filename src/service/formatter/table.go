package formatter

import (
	"fmt"
	"strings"

	"gitlab.com/tobidot/gitla/src/adapter/output"
	"gitlab.com/tobidot/gitla/src/domain"
)

func PrintPagedTable(table domain.Table) {
	out := output.PagedOutput{}
	out.Init()
	defer out.Close()

	maxColumnLength := getMaxColumnLengths(table)

	printRow(out, table.Columns, maxColumnLength)
	printHeaderSeparator(out, maxColumnLength)
	for _, row := range table.Rows {
		printRow(out, row, maxColumnLength)
	}
}

func printHeaderSeparator(output output.PagedOutput, maxColumnLength []int) {
	for i := range maxColumnLength {
		output.Write(" | ")
		output.Write(fmt.Sprint(strings.Repeat("-", maxColumnLength[i])))
	}
	output.Write(fmt.Sprintln(" | "))
}

func printRow(output output.PagedOutput, row []string, maxColumnLength []int) {
	for i, cell := range row {
		output.Write(" | ")
		output.Write(fmt.Sprintf("%-*s", maxColumnLength[i], cell))
	}
	output.Write(fmt.Sprintln(" | "))
}

func getMaxColumnLengths(table domain.Table) []int {
	maxColumnLength := make([]int, len(table.Columns))
	for _, row := range table.Rows {
		for i, column := range row {
			if len(column) > maxColumnLength[i] {
				maxColumnLength[i] = len(column)
			}
		}
	}
	return maxColumnLength
}
