package security

import (
	"fmt"
	"path/filepath"
	"strings"

	"gitlab.com/tobidot/gitla/src/service/config"
)

func IsInConfigPath(path string) error {
	path = filepath.Clean(path)

	if !strings.Contains(path, config.GetGitlaConfigDirPath()) {
		return fmt.Errorf("path is not in config directory")
	}
	return nil
}
