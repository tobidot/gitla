package find

import (
	"io/fs"
	"os"
	"path/filepath"
)

func GetAllGitRepositories(root string) ([]string, error) {
	var gitDirs []string
	r := func(path string, d fs.DirEntry, err error) error {
		if isDotDir(path, d) {
			return filepath.SkipDir
		}

		if isGitDir(path, d) {
			gitDirs = append(gitDirs, path)
			return filepath.SkipDir
		}
		return err
	}
	err := filepath.WalkDir(root, r)
	return gitDirs, err
}

func isDotDir(path string, d fs.DirEntry) bool {
	return d.IsDir() && filepath.Base(path)[0:1] == "."
}

func isGitDir(path string, d fs.DirEntry) bool {
	if !d.IsDir() {
		return false
	}
	git := filepath.Join(path, ".git")
	_, err := os.Stat(git)
	return err == nil
}
