package find

import (
	"os"
	"path"
	"testing"

	"gitlab.com/tobidot/gitla/src/testutils"
)

func Test_getAllGitRepositories(t *testing.T) {
	temp, _ := os.MkdirTemp("", "Test_getAllGitRepositories_testdir")
	defer func(name string) {
		_ = os.Remove(name)
	}(temp)
	topLevelProject := mkSubDir(temp, "top-level_project")
	projectRepo1 := mkFakeGitRepo(topLevelProject, "repo1")
	projectRepo2 := mkFakeGitRepo(topLevelProject, "repo2")

	mkSubDir(temp, "top-level_project_empty")

	topLevelRepo := mkFakeGitRepo(temp, "top-level_repo")

	dirs, err := GetAllGitRepositories(temp)
	testutils.AssertNil(t, err)
	testutils.AssertAllEq(t, dirs, []string{projectRepo1, projectRepo2, topLevelRepo})
}

func mkFakeGitRepo(pwd string, repoName string) string {
	repoPath := mkSubDir(pwd, repoName)
	mkSubDir(repoPath, ".git")
	return repoPath
}

func mkSubDir(pwd string, dirName string) string {
	subDirPath := path.Join(pwd, dirName)
	_ = os.Mkdir(subDirPath, 0755)
	return subDirPath
}
