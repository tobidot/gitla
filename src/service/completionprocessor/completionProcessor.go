package completionprocessor

import (
	"os"
	"strings"

	"gitlab.com/tobidot/gitla/src/domain"
)

func GetCompletions(queriesPath string, input domain.CompletionInput, flagOptions []string) []string {
	if strings.HasPrefix(input.PartialWord, "-") {
		return getCompletionsForFlagOptions(input, flagOptions)
	}

	for _, subCommand := range domain.SubCommands {
		if strings.Contains(input.FullLine, " "+string(subCommand)) {
			switch subCommand {
			case domain.Query:
				return completeFileNamesInPath(queriesPath, input)
			default:
				return []string{}
			}
		}
	}

	// if no subcommand was found in the previous loop
	return getSubcommandCompletions(input)
}

func completeFileNamesInPath(path string, input domain.CompletionInput) []string {
	var result []string
	queryFiles, err := getAllFilesInDir(path)
	if err != nil {
		return result
	}

	for _, file := range queryFiles {
		// if the file is already in the input, we don't want to suggest it again
		if strings.Contains(input.FullLine, file) {
			return []string{}
		}

		if strings.HasPrefix(file, input.PartialWord) {
			result = append(result, file)
		}
	}
	return result
}

func getAllFilesInDir(dir string) ([]string, error) {
	var result []string
	files, err := os.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	for _, file := range files {
		if !file.IsDir() {
			result = append(result, file.Name())
		}
	}
	return result, nil
}

func getSubcommandCompletions(input domain.CompletionInput) []string {
	var completions []string
	for _, sc := range domain.SubCommands {
		scString := string(sc)
		if strings.HasPrefix(scString, input.PartialWord) {
			completions = append(completions, scString)
		}
	}
	return completions
}

func getCompletionsForFlagOptions(input domain.CompletionInput, flagOptions []string) []string {
	var completions []string
	option := strings.TrimLeft(input.PartialWord, "-")
	for _, flagOption := range flagOptions {
		if option == "" || strings.HasPrefix(flagOption, option) {
			completions = append(completions, "-"+flagOption)
		}
	}
	return completions
}
