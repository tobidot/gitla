package completionprocessor

import (
	"reflect"
	"testing"

	"gitlab.com/tobidot/gitla/src/domain"
)

func TestGetCompletions(t *testing.T) {
	flagOptions := []string{"version", "p", "pprof"}
	allSubCommands := []string{"fetch", "log", "search", "createOrUpdateConfig", "query"}
	tests := []struct {
		name  string
		input domain.CompletionInput
		want  []string
	}{
		{
			name: "log command",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "l",
				FullLine:             "gitla l",
				PartialLine:          "gitla l",
				PrecedingLine:        "gitla",
			},
			want: []string{"log"},
		},
		{
			name: "fetch command",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "f",
				FullLine:             "gitla f",
				PartialLine:          "gitla f",
				PrecedingLine:        "gitla",
			},
			want: []string{"fetch"},
		},
		{
			name: "search command",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "s",
				FullLine:             "gitla s",
				PartialLine:          "gitla s",
				PrecedingLine:        "gitla",
			},
			want: []string{"search"},
		},
		{
			name: "All Options",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "-",
				FullLine:             "gitla -",
				PartialLine:          "gitla -",
				PrecedingLine:        "gitla",
			},
			want: []string{"-version", "-p", "-pprof"},
		},
		{
			name: "Option with multiple results",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "-p",
				FullLine:             "gitla -p",
				PartialLine:          "gitla -p",
				PrecedingLine:        "gitla",
			},
			want: []string{"-p", "-pprof"},
		},
		{
			name: "Option with on result",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "-v",
				FullLine:             "gitla -v",
				PartialLine:          "gitla -v",
				PrecedingLine:        "gitla",
			},
			want: []string{"-version"},
		},
		{
			name: "Command after Option",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "l",
				FullLine:             "gitla -p l",
				PartialLine:          "gitla -p l",
				PrecedingLine:        "gitla -p",
			},
			want: []string{"log"},
		},
		{
			name: "Option after command",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "-p",
				FullLine:             "gitla log -p",
				PartialLine:          "gitla log -p",
				PrecedingLine:        "gitla log",
			},
			want: []string{"-p", "-pprof"},
		},
		{
			name: "Cursor in the middle of line",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "l",
				FullLine:             "gitla l -p",
				PartialLine:          "gitla l",
				PrecedingLine:        "gitla",
			},
			want: []string{"log"},
		},
		{
			name: "Multiple commands are not completed",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "l",
				FullLine:             "gitla log l",
				PartialLine:          "gitla log l",
				PrecedingLine:        "gitla log",
			},
			want: []string{},
		},
		{
			name: "Multiple commands are not completed even if cursor in middle of line",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "l",
				FullLine:             "gitla l log",
				PartialLine:          "gitla l log",
				PrecedingLine:        "gitla",
			},
			want: []string{},
		},
		{
			name: "Command is completed even if option with same name already exists",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "l",
				FullLine:             "gitla -log l",
				PartialLine:          "gitla -log l",
				PrecedingLine:        "gitla -log",
			},
			want: []string{"log"},
		},
		{
			name: "No Options or commands yet shows all subcommands",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "",
				FullLine:             "gitla ",
				PartialLine:          "gitla ",
				PrecedingLine:        "gitla",
			},
			want: allSubCommands,
		},
		{
			name: "Empty partial word shows all subcommands even with options already present",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "",
				FullLine:             "gitla -p ",
				PartialLine:          "gitla -p ",
				PrecedingLine:        "gitla -p",
			},
			want: allSubCommands,
		},
		{
			name: "Empty query gives all queries",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "",
				FullLine:             "gitla query ",
				PartialLine:          "gitla query ",
				PrecedingLine:        "gitla query",
			},
			want: []string{"some_other_query", "some_query.sql"},
		},
		{
			name: "Initial letter of one query gives the query",
			input: domain.CompletionInput{
				CommandToBeCompleted: "gitla",
				PartialWord:          "some_q",
				FullLine:             "gitla query some_q",
				PartialLine:          "gitla query some_q",
				PrecedingLine:        "gitla query",
			},
			want: []string{"some_query.sql"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetCompletions("./test_data/queries", tt.input, flagOptions); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCompletions() = %v, want %v", got, tt.want)
			}
		})
	}
}
