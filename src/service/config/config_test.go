package config

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"gitlab.com/tobidot/gitla/src/adapter/jsonconfig"
	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/testutils"
)

func TestGetConfig(t *testing.T) {
	seed := time.Now().UnixNano()
	rand.New(rand.NewSource(seed))
	fmt.Printf("The seed for this test is: %d\n", seed)

	configRepo := &jsonconfig.JSONConfig{}

	t.Run("fail on missing Config", func(t *testing.T) {
		gitlaDir := filepath.Join(os.TempDir(), domain.UnsafeRandomString("gitla-test", 6), "gitla")
		want := domain.Config{}
		got, err := GetConfig(configRepo, gitlaDir)
		testutils.AssertErrorOfEqType(t, err, domain.ErrMissingConfig)
		if !reflect.DeepEqual(got, want) {
			t.Errorf("GetConfig() got = %v, want %v", got, want)
		}
	})

	t.Run("parse written config", func(t *testing.T) {
		customConfig := domain.Config{
			Root:   "custom-root",
			DBPath: "custom-db-path",
			LogParser: domain.LogParserConfig{
				LogParserDelimiterPrefix: "custom-delimiter",
			},
		}
		gitlaDir := filepath.Join(os.TempDir(), domain.UnsafeRandomString("gitla-test", 6), "gitla")
		jconf := &jsonconfig.JSONConfig{}
		want := customConfig

		err := os.MkdirAll(gitlaDir, 0700)
		testutils.AssertNil(t, err)
		err = jconf.WriteConfig(gitlaDir, customConfig)
		testutils.AssertNil(t, err)

		got, err := GetConfig(configRepo, gitlaDir)
		testutils.AssertNil(t, err)
		if !reflect.DeepEqual(got, want) {
			t.Errorf("GetConfig() got = %v, want %v", got, want)
		}
	})
}
