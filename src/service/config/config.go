package config

import (
	"errors"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/tobidot/gitla/src/domain"
)

const Dirname = "gitla"
const DBName = "commits.db"

func GetConfig(configRepo domain.ConfigRepository, gitlaConfigDir string) (domain.Config, error) {
	defaultConfig := NewDefaultConfig(gitlaConfigDir)
	return configRepo.ReadConfig(gitlaConfigDir, defaultConfig)
}

func GetGitlaConfigDirPath() string {
	generalConfigDir, _ := os.UserConfigDir()
	return filepath.Join(generalConfigDir, Dirname)
}

func GetGitlaQueryDirPath() string {
	return filepath.Join(GetGitlaConfigDirPath(), "queries")
}

func MakeConfigDirIfNotExists() error {
	gitlaDir := GetGitlaConfigDirPath()
	_, err := os.Stat(gitlaDir)
	if errors.Is(err, fs.ErrNotExist) {
		err = os.MkdirAll(gitlaDir, 0700)
		if err != nil {
			return err
		}
	}
	return nil
}

func NewDefaultConfig(dbDir string) domain.Config {
	homeDir, _ := os.UserHomeDir()
	dbPath := filepath.Join(dbDir, DBName)

	return domain.Config{
		Root:   homeDir,
		DBPath: dbPath,
		LogParser: domain.LogParserConfig{
			LogParserDelimiterPrefix: "€€€€€€",
		},
	}
}
