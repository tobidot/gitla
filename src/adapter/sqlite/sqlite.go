package sqlite

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3" // blank import used for enabling sqlite db
	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/service/security"
)

type Database struct {
	db *sql.DB
}

func (database *Database) Open(path string) error {
	dataSourceName := fmt.Sprintf("file:%s", path)
	var err error
	database.db, err = sql.Open("sqlite3", dataSourceName)
	return err
}

func (database *Database) Close() error {
	return database.db.Close()
}

func (database *Database) CreateCommitsTableIfNotExists() error {
	// Create table
	statement, err := database.db.Prepare(`
		CREATE TABLE IF NOT EXISTS commits (
		    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			commit_hash VARCHAR(128),
			parent_commit_hashes VARCHAR(512),
			is_on_main INTEGER,
			full_path VARCHAR(128),
			repo VARCHAR(128),
			parent VARCHAR(128),
			author_name VARCHAR(128),
			author_email VARCHAR(128),
		    author_date VARCHAR(128),
			committer_name VARCHAR(128),
			committer_email VARCHAR(128),
		    committer_date VARCHAR(128),
			subject VARCHAR(512),
			body VARCHAR(512),
		    diff VARCHAR(1024),
		    UNIQUE(full_path, commit_hash)
		)`)
	if err != nil {
		return err
	}
	_, err = statement.Exec()
	return err
}

func (database *Database) CreateFullTextSearchTableIfNotExists() error {
	// Create table
	statement, err := database.db.Prepare(`
		CREATE VIRTUAL TABLE IF NOT EXISTS commits_fts USING fts5(
			subject,
			body,
		    content='commits',
		    content_rowid='id'
		)`)
	if err != nil {
		return err
	}
	_, err = statement.Exec()
	return err
}

func (database *Database) CreateFullTextSearchTriggerIfNotExists() error {
	// Create table
	statement, err := database.db.Prepare(`
		CREATE TRIGGER IF NOT EXISTS commits_fts_after_insert AFTER INSERT ON commits
			BEGIN
				INSERT INTO commits_fts(
					rowid, 			
					subject,
					body
				)
				VALUES (new.id, new.subject, new.body);
			END;
        `)
	if err != nil {
		return err
	}
	_, err = statement.Exec()
	return err
}

//nolint:funlen // splitting the function would only increase complexity without any gain
func (database *Database) DumpCommitsToCommitsTable(all []domain.Entry) {
	tx, _ := database.db.Begin()
	statement, err := tx.Prepare(`
INSERT OR IGNORE INTO commits (
    commit_hash,
	parent_commit_hashes,
	is_on_main,
	full_path,
	repo,
	parent,
    author_name,
    author_email,
    author_date,
	committer_name,
    committer_email,
    committer_date,
    subject,
    body,
    diff
    )
    VALUES (
    @commit_hash,
	@parent_commit_hashes,
	@is_on_main,
	@full_path,
	@repo,
	@parent,
    @author_name,
    @author_email,
    @author_date,
    @committer_name,
    @committer_email,
    @committer_date,
    @subject,
    @body,
    @diff
    )`)
	if err != nil {
		log.Fatalln("Error preparing insert.", err)
	}

	for _, e := range all {
		_, err = statement.Exec(
			sql.Named("commit_hash", e.CommitHash),
			sql.Named("parent_commit_hashes", e.ParentHashes), // space separated
			sql.Named("is_on_main", -1),                       // default value to be overwritten afterwards
			sql.Named("full_path", e.FullPath),
			sql.Named("repo", e.Repo),
			sql.Named("parent", e.Parent),
			sql.Named("author_name", e.Author.Name),
			sql.Named("author_email", e.Author.Email),
			sql.Named("author_date", e.Author.Date),
			sql.Named("committer_name", e.Committer.Name),
			sql.Named("committer_email", e.Committer.Email),
			sql.Named("committer_date", e.Committer.Date),
			sql.Named("subject", e.Subject),
			sql.Named("body", e.Body),
			sql.Named("diff", e.Diff),
		)
		if err != nil {
			log.Fatalln("Error inserting", err)
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatalln("Error preparing insert.", err)
	}
}

func (database *Database) MarkCommitsAsOnMain(mainHashes []string, path string) {
	tx, err := database.db.Begin()
	if err != nil {
		log.Fatalln("Error starting transaction", err)
	}

	_, err = tx.Exec("UPDATE commits SET is_on_main = 0 WHERE is_on_main = -1")
	if err != nil {
		log.Fatalln("Error resetting main branch flags", err)
	}

	for _, hash := range mainHashes {
		_, err = tx.Exec("UPDATE commits SET is_on_main = 1 WHERE full_path = ? AND commit_hash = ?", path, hash)
		if err != nil {
			log.Fatalln("Error marking commit as on main", err)
		}
	}

	err = tx.Commit()
	if err != nil {
		log.Fatalln("Error committing transaction", err)
	}
}

func (database *Database) GetLastCommitDateForPath(path string) []domain.LastCommit {
	// get all commits with the last committer_date and the full_path
	queryResult, err := database.db.Query(`
SELECT commit_hash, committer_date FROM commits
  WHERE full_path = ? 
    AND datetime(committer_date) = (SELECT MAX(datetime(committer_date)) 
                                    FROM main.commits
                                    WHERE full_path = ?
    )
    `, path, path)
	if err != nil {
		log.Fatalln(err)
	}
	var lastCommits []domain.LastCommit
	for queryResult.Next() {
		var lastCommit domain.LastCommit
		err := queryResult.Scan(&lastCommit.CommitHash, &lastCommit.CommitDate)
		if err != nil {
			log.Fatalln(err)
		}
		lastCommits = append(lastCommits, lastCommit)
	}
	return lastCommits
}

func (database *Database) QueryFTS(query string) (domain.Entries, error) {
	queryResult, err := database.db.Query(`
SELECT 
    commit_hash,
	full_path,
	repo,
	parent,
    author_name,
    author_email,
    author_date,
	committer_name,
    committer_email,
    committer_date,
    subject,
    body,
    diff
FROM commits 
         WHERE ROWID IN (
             SELECT ROWID FROM commits_fts WHERE commits_fts MATCH ? ORDER BY rank
         );`, query)
	if err != nil {
		return domain.Entries{}, err
	}
	entries := domain.Entries{Entries: []domain.Entry{}}
	for queryResult.Next() {
		var entry domain.Entry
		err := queryResult.Scan(
			&entry.CommitHash,
			&entry.FullPath,
			&entry.Repo,
			&entry.Parent,
			&entry.Author.Name,
			&entry.Author.Email,
			&entry.Author.Date,
			&entry.Committer.Name,
			&entry.Committer.Email,
			&entry.Committer.Date,
			&entry.Subject,
			&entry.Body,
			&entry.Diff,
		)
		if err != nil {
			return domain.Entries{}, err
		}
		entries.Entries = append(entries.Entries, entry)
	}
	return entries, nil
}

func (database *Database) QueryFile(filePath string) (domain.Table, error) {
	err := security.IsInConfigPath(filePath)
	if err != nil {
		panic(err)
	}
	query, err := os.ReadFile(filePath) // #nosec G304 // as path is validated above
	if err != nil {
		return domain.Table{}, err
	}
	queryResult, err := database.db.Query(string(query))
	if err != nil {
		return domain.Table{}, err
	}

	columns, err := queryResult.Columns()
	if err != nil {
		log.Fatal(err)
	}

	scanArgs := make([]interface{}, len(columns))
	for i := range scanArgs {
		scanArgs[i] = new(sql.NullString)
	}
	var rows [][]string
	// Iterate through the result set
	for queryResult.Next() {
		// reset the scanArgs
		for i := range scanArgs {
			scanArgs[i] = new(sql.NullString)
		}

		err := queryResult.Scan(scanArgs...)
		if err != nil {
			log.Fatal(err)
		}

		// Convert to a slice of strings
		row := make([]string, len(columns))
		for i, v := range scanArgs {
			// Type assert back to sql.NullString and take the String value
			row[i] = v.(*sql.NullString).String
		}
		rows = append(rows, row)
	}
	return domain.Table{Columns: columns, Rows: rows}, nil
}
