package sqlite

import (
	"database/sql"
	"os"
	"testing"
	"time"

	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/testutils"
)

const DBName = "commits.db"

func TestDatabase_CreateTableIfNotExists(t *testing.T) {
	db := Database{}
	path := DBName
	err := db.Open(path)
	testutils.AssertNil(t, err)

	defer func() {
		err = db.Close()
		testutils.AssertNil(t, err)
		_ = os.Remove(path)
	}()

	err = db.CreateCommitsTableIfNotExists()
	err = db.CreateFullTextSearchTableIfNotExists()
	err = db.CreateFullTextSearchTriggerIfNotExists()

	testutils.AssertNil(t, err)
}

func TestDatabase_DumpCommitsToDb(t *testing.T) {
	db := Database{}
	path := DBName
	_ = db.Open(path)

	defer func() {
		_ = db.Close()
		_ = os.Remove(path)
	}()

	_ = db.CreateCommitsTableIfNotExists()
	_ = db.CreateFullTextSearchTableIfNotExists()
	_ = db.CreateFullTextSearchTriggerIfNotExists()

	db.DumpCommitsToCommitsTable(domain.ProvideRandomEntries().Entries)

	rows, err := db.db.Query(`SELECT * FROM commits`)
	testutils.AssertNil(t, err)
	defer func(rows *sql.Rows) {
		_ = rows.Close()
	}(rows)

	var counter int8
	for rows.Next() {
		counter++
	}
	testutils.AssertEq(t, counter, 3)
}

func TestDatabase_GetLastCommitForPath(t *testing.T) {
	db := Database{}
	path := DBName
	_ = db.Open(path)

	defer func() {
		_ = db.Close()
		_ = os.Remove(path)
	}()

	_ = db.CreateCommitsTableIfNotExists()
	_ = db.CreateFullTextSearchTableIfNotExists()
	_ = db.CreateFullTextSearchTriggerIfNotExists()

	t.Run("find last commit", func(t *testing.T) {
		entries := domain.ProvideRandomEntries().Entries
		gitPath := domain.UnsafeRandomString("/test/path/", 3)
		lastDate := time.Date(1970, 1, 0, 0, 0, 1, 0, time.UTC).Format(time.RFC3339)
		otherDate := time.Date(1970, 1, 0, 0, 0, 0, 0, time.UTC).Format(time.RFC3339)
		entries[0].FullPath = gitPath
		entries[0].Committer.Date = otherDate
		entries[1].FullPath = gitPath
		entries[1].Committer.Date = lastDate
		entries[2].FullPath = gitPath
		entries[2].Committer.Date = otherDate
		db.DumpCommitsToCommitsTable(entries)

		lastCommits := db.GetLastCommitDateForPath(gitPath)
		testutils.AssertEq(t, len(lastCommits), 1)
		testutils.AssertEq(t, lastCommits[0].CommitDate, entries[1].Committer.Date)
		testutils.AssertEq(t, lastCommits[0].CommitHash, entries[1].CommitHash)
	})

	t.Run("find multiple last commits", func(t *testing.T) {
		entries := domain.ProvideRandomEntries().Entries
		gitPath := domain.UnsafeRandomString("/test/path/", 3)
		lastDate := time.Date(1970, 1, 0, 0, 0, 1, 0, time.UTC).Format(time.RFC3339)
		otherDate := time.Date(1970, 1, 0, 0, 0, 0, 0, time.UTC).Format(time.RFC3339)
		entries[0].FullPath = gitPath
		entries[0].Committer.Date = lastDate
		entries[1].FullPath = gitPath
		entries[1].Committer.Date = lastDate
		entries[2].FullPath = gitPath
		entries[2].Committer.Date = otherDate
		db.DumpCommitsToCommitsTable(entries)

		lastCommits := db.GetLastCommitDateForPath(gitPath)
		testutils.AssertEq(t, len(lastCommits), 2)
		testutils.AssertEq(t, lastCommits[0].CommitDate, entries[0].Committer.Date)
		testutils.AssertEq(t, lastCommits[1].CommitDate, entries[0].Committer.Date)
		testutils.AssertContainsBy(t, lastCommits, entries[0].CommitHash, func(commit domain.LastCommit) string { return commit.CommitHash })
		testutils.AssertContainsBy(t, lastCommits, entries[1].CommitHash, func(commit domain.LastCommit) string { return commit.CommitHash })
	})

	t.Run("find nothing other path", func(t *testing.T) {
		gitPath := domain.UnsafeRandomString("/test/path/", 3)

		lastCommits := db.GetLastCommitDateForPath(gitPath)
		testutils.AssertEq(t, len(lastCommits), 0)
	})
}

func TestDatabase_QueryFTS(t *testing.T) {
	db := Database{}
	path := DBName
	_ = db.Open(path)

	defer func() {
		_ = db.Close()
		_ = os.Remove(path)
	}()

	_ = db.CreateCommitsTableIfNotExists()
	_ = db.CreateFullTextSearchTableIfNotExists()
	_ = db.CreateFullTextSearchTriggerIfNotExists()

	t.Run("find commit by token", func(t *testing.T) {
		entries := domain.ProvideRandomEntries().Entries
		entries[1].Subject = "This is the token I'm looking for."
		db.DumpCommitsToCommitsTable(entries)

		result, err := db.QueryFTS("token")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, len(result.Entries), 1)
		testutils.AssertEq(t, result.Entries[0].CommitHash, entries[1].CommitHash)
	})

	t.Run("find commit by AND query", func(t *testing.T) {
		entries := domain.ProvideRandomEntries().Entries
		entries[0].Subject = "This is token"
		entries[1].Subject = "This is really the token I'm looking for."
		entries[2].Subject = "token"
		db.DumpCommitsToCommitsTable(entries)

		result, err := db.QueryFTS("token AND really")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, len(result.Entries), 1)
		testutils.AssertEq(t, result.Entries[0].CommitHash, entries[1].CommitHash)
	})

	t.Run("find commit by wildcard", func(t *testing.T) {
		entries := domain.ProvideRandomEntries().Entries
		entries[1].Subject = "A Ridiculouslylongword."
		db.DumpCommitsToCommitsTable(entries)

		result, err := db.QueryFTS("ridicul*")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, len(result.Entries), 1)
		testutils.AssertEq(t, result.Entries[0].CommitHash, entries[1].CommitHash)
	})
}
