package shell

import (
	"bytes"
	"os"
	"os/exec"
)

type Bash struct {
}

func (*Bash) RunCommandInPath(path string, command string) (string, string, error) {
	switchDirectory := "cd " + path
	//#nosec G204 -- Subprocess launched with a potential tainted input or cmd arguments
	// The commands are not user supplied hence it's assumed to be safe
	cmd := exec.Command("/bin/bash", "-c", switchDirectory+";"+command)
	cmd.Env = os.Environ()

	stdout := &bytes.Buffer{}
	cmd.Stdout = stdout

	stderr := &bytes.Buffer{}
	cmd.Stderr = stderr

	err := cmd.Run()

	return stdout.String(), stderr.String(), err
}
