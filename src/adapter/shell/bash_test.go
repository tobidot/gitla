package shell

import (
	"os"
	"testing"

	"gitlab.com/tobidot/gitla/src/testutils"
)

func TestBash_RunCommandInPath(t *testing.T) {
	t.Run("simple echo is successful", func(t *testing.T) {
		bash := Bash{}
		stdout, stderr, err := bash.RunCommandInPath(".", "echo test")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, stdout, "test\n")
		testutils.AssertEq(t, stderr, "")
	})
	t.Run("simple Environment variables are passed", func(t *testing.T) {
		_ = os.Setenv("gitla_test_environ", "test_environ")
		bash := Bash{}
		stdout, stderr, err := bash.RunCommandInPath(".", "echo $gitla_test_environ")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, stdout, "test_environ\n")
		testutils.AssertEq(t, stderr, "")
	})
	t.Run("Write to error", func(t *testing.T) {
		bash := Bash{}
		stdout, stderr, err := bash.RunCommandInPath(".", "echo \"error\" 1>&2")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, stdout, "")
		testutils.AssertEq(t, stderr, "error\n")
	})
	t.Run("Write to error", func(t *testing.T) {
		bash := Bash{}
		stdout, stderr, err := bash.RunCommandInPath(".", "exit")
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, stdout, "")
		testutils.AssertEq(t, stderr, "")
	})
}
