package jsonconfig

import (
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/service/config"
	"gitlab.com/tobidot/gitla/src/testutils"
)

func TestJsonConfig_ReadConfig(t *testing.T) {
	jsonConfig := JSONConfig{}
	defaultConfig := config.NewDefaultConfig("/default/db/dir")

	t.Run("non existent file throws error", func(t *testing.T) {
		_, err := jsonConfig.ReadConfig("./test_data/this-does-not-exist", defaultConfig)
		testutils.AssertErrorOfEqType(t, err, domain.ErrMissingConfig)
	})

	t.Run("file with invalid json throws error", func(t *testing.T) {
		_, err := jsonConfig.ReadConfig("./test_data/invalid_json", defaultConfig)
		testutils.AssertErrorNotOfType(t, err, domain.ErrMissingConfig)
	})

	t.Run("empty json gives default", func(t *testing.T) {
		c, err := jsonConfig.ReadConfig("./test_data/empty_json", defaultConfig)
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, c, defaultConfig)
	})

	t.Run("missing properties are filled in", func(t *testing.T) {
		c, err := jsonConfig.ReadConfig("./test_data/missing_values_json", defaultConfig)
		testutils.AssertNil(t, err)

		expectedConfig := defaultConfig
		expectedConfig.Root = "/home/root"
		testutils.AssertEq(t, c, expectedConfig)
	})

	t.Run("complete json is read", func(t *testing.T) {
		c, err := jsonConfig.ReadConfig("./test_data/full_config_json", defaultConfig)
		testutils.AssertNil(t, err)

		expectedConfig := domain.Config{
			Root:   "/home/root",
			DBPath: "/home/root/db/Path/",
			LogParser: domain.LogParserConfig{
				LogParserDelimiterPrefix: "never gonna be used in commit messages",
			},
		}
		testutils.AssertEq(t, c, expectedConfig)
	})

	t.Run("unknown json properties are ignored", func(t *testing.T) {
		c, err := jsonConfig.ReadConfig("./test_data/full_config_with_additional_properties", defaultConfig)
		testutils.AssertNil(t, err)

		expectedConfig := domain.Config{
			Root:   "/home/root",
			DBPath: "/home/root/db/Path/",
			LogParser: domain.LogParserConfig{
				LogParserDelimiterPrefix: "never gonna be used in commit messages",
			},
		}
		testutils.AssertEq(t, c, expectedConfig)
	})
}

func TestJsonConfig_WriteConfig(t *testing.T) {
	jsonConfig := JSONConfig{}
	defaultConfig := config.NewDefaultConfig("/default/db/dir")

	configToWrite := domain.Config{
		Root:   "/home/root",
		DBPath: "/home/root/db/Path/",
		LogParser: domain.LogParserConfig{
			LogParserDelimiterPrefix: "never gonna be used in commit messages",
		},
	}

	t.Run("non existent file throws error", func(t *testing.T) {
		dirPath := "./test_data/write_test"
		err := os.MkdirAll(dirPath, 0700)
		testutils.AssertNil(t, err)

		configFilePath := filepath.Join(dirPath, "conf.json")
		defer func() {
			err = os.Remove(configFilePath)
			testutils.AssertNil(t, err)
		}()

		err = jsonConfig.WriteConfig(dirPath, configToWrite)
		testutils.AssertNil(t, err)

		parsedConfig, err := jsonConfig.ReadConfig(dirPath, defaultConfig)
		testutils.AssertNil(t, err)
		testutils.AssertEq(t, configToWrite, parsedConfig)
	})
}
