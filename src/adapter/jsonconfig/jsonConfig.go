package jsonconfig

import (
	"encoding/json"
	"errors"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/tobidot/gitla/src/domain"
)

const ConfigFileName = "conf.json"

type JSONConfig struct{}

func (jc *JSONConfig) WriteConfig(configDirPath string, config domain.Config) error {
	jsonDump, err := json.MarshalIndent(config, "", "  ")
	if err != nil {
		return err
	}

	path := getConfigFilePath(configDirPath)
	err = os.WriteFile(path, jsonDump, 0600)
	if err != nil {
		return err
	}
	return nil
}

func (jc *JSONConfig) ReadConfig(configDirPath string, defaultConfig domain.Config) (domain.Config, error) {
	path := getConfigFilePath(configDirPath)
	if _, err := os.Stat(path); errors.Is(err, fs.ErrNotExist) {
		return domain.Config{}, domain.ErrMissingConfig
	}

	//#nosec G304 -- Potential file inclusion via variable
	raw, err := os.ReadFile(path)
	if err != nil {
		return domain.Config{}, err
	}
	var config = defaultConfig
	err = json.Unmarshal(raw, &config)
	if err != nil {
		return domain.Config{}, err
	}

	return config, nil
}

func getConfigFilePath(configDirPath string) string {
	return filepath.Join(configDirPath, ConfigFileName)
}
