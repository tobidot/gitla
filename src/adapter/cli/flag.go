package cli

import (
	"flag"
	"fmt"

	"gitlab.com/tobidot/gitla/src/domain"
)

type FlagOptions struct {
	IsVersion     bool
	IsPerformance bool
}

type ParsedArgs struct {
	SubCommand                     domain.SubCommand
	Arguments                      []string
	IsVersion                      bool
	IsPerformanceCollectionEnabled bool
}

type ParseError struct {
	reason string
	usage  string
}

func (e *ParseError) Error() string {
	return fmt.Sprintf("Arguments could not be parsed because %s\n\n%s", e.reason, e.usage)
}

const usage = `
gitla [OPTIONS] SUBCOMMAND [SUBCOMMAND_ARGUMENTS]

SUBCOMMAND:
 fetch             Fetch latest state from origin in all git repositories using git fetch
 log               Collect logs in sqlite database commits.db in the current working directory
 search '[TERM]'   Search for the TERM in the commit-message (subject/body) using sqlite fts5

OPTIONS:
 -p, --pprof          Collect pprof performance data
 --version            Version and other build information
`

func NewGitlaFlags() (*flag.FlagSet, *FlagOptions) {
	// Using a NewFlagSet prevents the panic due to "flag redefined" when running tests
	flags := flag.NewFlagSet("default", flag.ExitOnError)
	flagOptions := &FlagOptions{false, false}

	flags.BoolVar(&flagOptions.IsPerformance, "pprof", false, "Collect pprof performance data")
	flags.BoolVar(&flagOptions.IsPerformance, "p", false, "Collect pprof performance data")
	flags.BoolVar(&flagOptions.IsVersion, "version", false, "Print version and additional information on build")
	return flags, flagOptions
}

func ParseCLI(osArgs []string, flags *flag.FlagSet, flagOptions *FlagOptions) (ParsedArgs, *ParseError) {
	err := flags.Parse(osArgs[1:])
	if err != nil {
		return ParsedArgs{}, &ParseError{err.Error(), usage}
	}
	if flagOptions.IsVersion {
		return ParsedArgs{}, nil
	}

	values := flags.Args()
	if len(values) == 0 {
		return ParsedArgs{}, &ParseError{"Exactly one subcommand must be given.", usage}
	}
	command, err := parseSubCommand(values[0])
	if err != nil {
		reason := fmt.Sprintf("No valid subcommand found for %s", values[0])
		return ParsedArgs{}, &ParseError{reason, usage}
	}

	return ParsedArgs{
		IsPerformanceCollectionEnabled: flagOptions.IsPerformance,
		SubCommand:                     command,
		Arguments:                      values[1:],
	}, nil
}

func parseSubCommand(subCommand string) (domain.SubCommand, error) {
	switch subCommand {
	case string(domain.CreateOrUpdateConfig):
		return domain.CreateOrUpdateConfig, nil
	case string(domain.Log):
		return domain.Log, nil
	case string(domain.Fetch):
		return domain.Fetch, nil
	case string(domain.Search):
		return domain.Search, nil
	case string(domain.Query):
		return domain.Query, nil
	default:
		return "", fmt.Errorf("%s is no valid subcommand", subCommand)
	}
}
