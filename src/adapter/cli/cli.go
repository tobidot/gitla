package cli

import (
	"errors"
	"flag"
	"fmt"
	"os"

	"github.com/mmcloughlin/profile"
	"gitlab.com/tobidot/gitla/src/adapter/completion"
	"gitlab.com/tobidot/gitla/src/adapter/jsonconfig"
	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/service/config"
	"gitlab.com/tobidot/gitla/src/subcommands"
)

func RunCli(version string, commit string) (err error) {
	bashComplete := completion.BashCompletion{}
	flags, flagBuffer := NewGitlaFlags()

	var flagOptions []string
	flags.VisitAll(func(flag *flag.Flag) {
		flagOptions = append(flagOptions, flag.Name)
	})
	bashComplete.Complete(config.GetGitlaQueryDirPath(), os.Args, flagOptions)

	args, cliErr := ParseCLI(os.Args, flags, flagBuffer)
	if cliErr != nil {
		fmt.Print(cliErr.Error())
		os.Exit(1)
	}
	if args.IsVersion {
		fmt.Println("Version: ", version)
		fmt.Println("Commit:  ", commit)
		fmt.Printf("src:      https://gitlab.com/tobidot/gitla/-/commit/%s", commit)
	}

	if args.IsPerformanceCollectionEnabled {
		defer profile.Start(profile.CPUProfile, profile.MemProfile).Stop()
	}

	conf, err := config.GetConfig(&jsonconfig.JSONConfig{}, config.GetGitlaConfigDirPath())
	if err != nil {
		if errors.Is(err, domain.ErrMissingConfig) {
			if args.SubCommand != domain.CreateOrUpdateConfig {
				return fmt.Errorf("you need to create a config before running gitla")
			}
		} else {
			return fmt.Errorf("error while obtaining config: %v", err)
		}
	}

	switch args.SubCommand {
	case domain.CreateOrUpdateConfig:
		return subcommands.CreateOrUpdateConfig(&jsonconfig.JSONConfig{}, conf)
	case domain.Fetch:
		return subcommands.Fetch(conf.Root)
	case domain.Log:
		return subcommands.LogToDB(conf.Root, conf.DBPath, conf.LogParser)
	case domain.Search:
		if len(args.Arguments) != 1 {
			return fmt.Errorf("to many arguments provided: %v", args.Arguments)
		}
		return subcommands.Search(conf.DBPath, args.Arguments[0])
	case domain.Query:
		if len(args.Arguments) != 1 {
			return fmt.Errorf("to many arguments provided: %v", args.Arguments)
		}
		return subcommands.Query(conf.DBPath, args.Arguments[0])
	default:
		return nil
	}
}
