package cli

import (
	"strings"
	"testing"

	"gitlab.com/tobidot/gitla/src/domain"
)

func TestFetch(t *testing.T) {
	flags, flagOptions := NewGitlaFlags()
	osArgs := []string{"gitla", "fetch"}

	parsedArgs, err := ParseCLI(osArgs, flags, flagOptions)
	assert(t, err == nil)
	assert(t, parsedArgs.SubCommand == domain.Fetch)
}

func TestLog(t *testing.T) {
	flags, flagOptions := NewGitlaFlags()
	osArgs := []string{"gitla", "log"}

	parsedArgs, err := ParseCLI(osArgs, flags, flagOptions)
	assert(t, err == nil)
	assert(t, parsedArgs.SubCommand == domain.Log)
}

func TestBogus(t *testing.T) {
	flags, flagOptions := NewGitlaFlags()
	osArgs := []string{"gitla", "bogus"}

	parsedArgs, err := ParseCLI(osArgs, flags, flagOptions)
	assert(t, strings.Contains(err.Error(), usage))
	assert(t, parsedArgs.SubCommand == "")
}

func TestTooManySubcommand(t *testing.T) {
	flags, flagOptions := NewGitlaFlags()
	osArgs := []string{"gitla", "bogus", "foo"}

	parsedArgs, err := ParseCLI(osArgs, flags, flagOptions)
	assert(t, strings.Contains(err.Error(), usage))
	assert(t, parsedArgs.SubCommand == "")
}

func assert(t *testing.T, exp bool) {
	if !exp {
		t.Error(exp)
	}
}
