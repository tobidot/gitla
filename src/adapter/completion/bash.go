package completion

// inspiration https://blog.jbowen.dev/2019/11/bash-command-line-completion-with-go/

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/tobidot/gitla/src/domain"
	"gitlab.com/tobidot/gitla/src/service/completionprocessor"
)

type BashCompletion struct{}

func (*BashCompletion) Complete(queriesPath string, args []string, flagOptions []string) {
	input, isCompletionInvocation, err := parseBashInput(args)
	if !isCompletionInvocation {
		return
	}
	if err != nil {
		os.Exit(1)
	}

	completions := completionprocessor.GetCompletions(queriesPath, input, flagOptions)
	if len(completions) == 0 {
		os.Exit(1)
	}

	for _, completion := range completions {
		fmt.Println(completion)
	}
	os.Exit(0)
}

func parseBashInput(args []string) (domain.CompletionInput, bool, error) {
	// https://www.gnu.org/software/bash/manual/bash.html#index-COMP_005fLINE
	// This environment variable is only set if called for completion
	fullLine, ok := os.LookupEnv("COMP_LINE")
	if !ok {
		return domain.CompletionInput{}, false, nil
	}

	// https://www.gnu.org/software/bash/manual/bash.html#index-COMP_005fPOINT
	rawPoint, ok := os.LookupEnv("COMP_POINT")
	if !ok {
		return domain.CompletionInput{}, true, fmt.Errorf("the COMP_POINT is not present even though COMP_LINE is set to %s", fullLine)
	}
	point, err := strconv.ParseInt(rawPoint, 10, 0)
	if err != nil {
		return domain.CompletionInput{}, true, fmt.Errorf("the COMP_POINT %s cannot be parsed as integer", rawPoint)
	}
	partialWord := args[2]

	// See https://www.gnu.org/software/bash/manual/bash.html#Programmable-Completion-1
	// args[0] -> the program called to return the completions
	// args[1] -> the command being completed
	// args[2] -> the partial word at cursor which should be completed
	// args[2] -> the word preceding the partial word
	input := domain.CompletionInput{
		CommandToBeCompleted: args[1],
		PartialWord:          partialWord,
		FullLine:             fullLine,
		PartialLine:          fullLine[:point],
		PrecedingLine:        strings.TrimSuffix(fullLine[:point], partialWord),
	}
	return input, true, nil
}
