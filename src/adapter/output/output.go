package output

import (
	"errors"
	"fmt"
	"io"
	"log/slog"
	"os"
	"os/exec"

	"golang.org/x/term"
)

type Type string

const (
	StdOut Type = "stdout"
	Pipe   Type = "pipe"
)

type PagedOutput struct {
	Writer              io.Writer
	pagerClosingChannel chan struct{}
	Type                Type
}

func (p *PagedOutput) Init() {
	if !term.IsTerminal(int(os.Stdout.Fd())) {
		p.Type = StdOut
		p.Writer = os.Stdout
		return
	}
	p.Type = Pipe
	// Add --redraw-on-quit once less version newer than 598 is on Ubuntu LTS
	// -X: sadly does more than just redrawing on quit and results in very verbose output when scrolling horizontally
	// see: https://superuser.com/a/1775328
	cmd := exec.Command("less", "--chop-long-lines", "--quit-if-one-screen")

	// Create a pipe to write output to
	var pipeIn *io.PipeReader
	pipeIn, p.Writer = io.Pipe()
	cmd.Stdin = pipeIn
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	p.pagerClosingChannel = make(chan struct{})
	go func() {
		slog.Debug("Running pager")
		defer close(p.pagerClosingChannel)
		// Close pipeIn when cmd.Run() returns to unblock the writer
		defer func() {
			err := pipeIn.Close()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to close pipe: %v\n", err)
			}
		}()

		if err := cmd.Run(); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to run pager: %v\n", err)
			return
		}
		slog.Debug("Finishing pager")
	}()
}

func (p *PagedOutput) Write(s string) {
	_, err := io.WriteString(p.Writer, s)
	if err != nil {
		// This is required to prevent the pipeline from blocking, which would happen
		// if the pager command is closed before it read everything from stdin
		// (i.e. when pressing `q` before scrolling to end of output)
		if errors.Is(err, io.ErrClosedPipe) {
			return
		}

		// This is not expected to happen
		fmt.Fprint(os.Stderr, err)
	}
}

func (p *PagedOutput) Close() {
	slog.Debug("Closing PagedOutput")
	if pipeWriter, ok := p.Writer.(*io.PipeWriter); ok {
		slog.Debug("Waiting for pager to close")
		slog.Debug("Closing pipe")

		err := pipeWriter.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to close pipe: %v\n", err)
		}

		// Wait for pager to close
		<-p.pagerClosingChannel
		return
	}
	slog.Debug("No pipe to close")
}
