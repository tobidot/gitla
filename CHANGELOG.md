# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this p

## [0.3.0] - 2025-03-06

### Added
 - Include basic queries during installation to promote `gitla query <query>` feature.
 - **Breaking:** Track which branches are on the `main`/`trunk` branch based on which commits are (first) parent commits of `origin/HEAD`.
   - For an example on how this can be used see `data/queries/install/trunk_based_top10.sql`.
   - This change is breaking as it includes adding a new column to the database. To migrate to the new version delete the `~/.config/gitla/commits.db` file and recreate the database using `gitla log`.

### Changed
 - No longer create configuration file if it not already exists on any command. Only create configuration when called with `gitla createOrUpdatedConfig`.
 - No longer automatically activate the systemd timer at installation. Only document it in the `README.md`.

### Fixed
 - Log all commits, as in `git log --all`, in the `commits.db`. 
   This is particularly relevant for the new `main`/`trunk` branch tracking, as the implementation hinges on the assumption that all commits are tracked. 

### Documentation
 - Update Documentation on using `gitla query` in manpages and README.

## [0.2.2] - 2025-02-16

### Documentation

 - Improve name of install script.

## [0.2.1] - 2025-02-16

### Documentation

 - Fix outdated download links for release.

## [0.2.0] - 2025-01-26

### Changed

 - Simplify the structure of the projects build scripts, since the binary installation makes the old structure/scripts obsolete.

## [0.1.9] - 2025-01-26

### Added

 - Add static linking to the build process.
   This allows to build a static binary that can be used on systems without the right glibc version.

## [0.1.8] - 2025-01-26

### Fixed

 - Add missing link for install script to release page.

## [0.1.7] - 2025-01-26

### Added

 - Implement install of binary release artifacts using bash script.
   This allows users to install gitla without having to build it from source.

## [0.1.6] - 2025-01-26

### Added

 - Add install files tarball to the release job output.

## [0.1.5] - 2025-01-26

### Fixed

 - Finally fix the release job.

## [0.1.4] - 2025-01-25

### Fixed

 - Fix `gzip` permission error in release job.

## [0.1.3] - 2025-01-25

### Fixed

 - Fix `gzip` command in release job only knowing `-k` but not `--keep`.

## [0.1.2] - 2025-01-25

### Added

 - Add new `gitla query <query-file-name>` command.
   This command allows to run custom sql queries on the database.
   - automatically pass output to a pager (for now only `less`)
 - Add tarball of all install relevant files to the release job.

### Changed
 - Use go 1.21 and update golangci-lint setup

## [0.1.1] - 2023-02-12

### Fixed

- Fix bug in function getting last commit date for a repo.
  The bug would only affect repos containing commits  with `committer_date != author_date`.
  To make sure that you're not missing any logs in your db it is recommended to rebuild the database form scratch.
  You can do this by `rm $your_db` followed by `gitla log`.

### Changed

- Improve performance and output for `git log`
  - Prior to this change for every repository the last commit was always attempted to be written to the database which then filtered them out by using key constraints.

    This led to misleading command line output for `git log`, namely `Dumping 1 new commits` even if no truly new commit was there.

  - Now they are filtered out at the application level.

    This results in clearer command line output `Adding 0 new commits` for up-to-date repositories.
    Furthermore, it results in a speedup as the database is not touched for up-to-date repositories.

## [0.1.0] - 2023-01-29

### Added

- Initial version of the project
  - basic config support 
    - using json
    - allowing incomplete configs
    - allowing superfluous config properties
    - writing config if none exists yet
  - man page
  - basic install scripts
  - basic systemd timers/units
  - bash completions
  - basic commands
    - fetch
    - log
    - search
    - createOrUpdateConfig

[0.3.0]: https://gitlab.com/tobidot/gitla/-/compare/v0.2.2...v0.3.0
[0.2.2]: https://gitlab.com/tobidot/gitla/-/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/tobidot/gitla/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.9...v0.2.0
[0.1.9]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.8...v0.1.9
[0.1.8]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/tobidot/gitla/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/tobidot/gitla/-/compare/9c9bfbf730fb6ab48176261853d981dff9cb35d1...v0.1.0
