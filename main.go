package main

import (
	"fmt"
	"os"

	"gitlab.com/tobidot/gitla/src/adapter/cli"
)

// Version will be overwritten by ldflags during build
var Version = "dev-version"

// Commit will be overwritten by ldflags during build
var Commit = "dev-commit-hash"

func main() {
	err := cli.RunCli(Version, Commit)
	if err != nil {
		fmt.Printf("An Error occurred: %v", err)
		os.Exit(1)
	}
}
