#!/bin/bash
set -euxo pipefail

archive_folder="gitla-install-files"
archive_file="${archive_folder}.tar.gz"
archive_url="${PACKAGE_REGISTRY_URL}/${archive_file}"

for cmd in curl gzip systemctl tar install; do
  if ! command -v "$cmd" &> /dev/null; then
    echo "Error: $cmd is not installed."
    exit 1
  fi
done

# Create temporary directory
tmp_dir=$(mktemp -d)
trap 'rm -rf "$tmp_dir"' EXIT
cd "$tmp_dir"

# Define installation directories
bin_folder="$HOME/.local/bin"
completion_dir="$HOME/.bash_completions"
man_folder="$HOME/.local/share/man/man1"

# Download archive and extract
echo "Downloading gitla binary..."
curl -L -o "${archive_file}" "${archive_url}"

# Verify download was successful before proceeding
if [ ! -f "${archive_file}" ]; then
    echo "Error: Failed to download gitla binary"
    exit 1
fi

mkdir -p "${archive_folder}"
tar -xzf "${archive_file}" -C "${archive_folder}"
ls -l "${archive_folder}"

echo "Installing gitla binary..."
install -Dm755 "${archive_folder}/gitla" "$bin_folder/gitla"


# This only has an effect if the following is part of your .bashrc
#
# # Source general completions
# completions_folder="$HOME/.bash_completions"
# if [ -d "$completions_folder" ]
#   for f in "$completions_folder"/*.sh; do source "$f"; done;
# fi
echo "Installing bash completions..."
install -Dm644 "${archive_folder}/completions/bash.sh" "$completion_dir/gitla.sh"

echo "Installing man page..."
install -Dm644 "${archive_folder}/build/docs/gitla.1.gz" "$man_folder/gitla.1.gz"

echo "Installing packaged queries..."
for file in "${archive_folder}"/data/queries/install/*; do
    install -Dm644 -t "$HOME/.config/gitla/queries" "$file" 
done

echo "Installing systemd service and timer..."
install -Dm644 "${archive_folder}/systemd/gitla-log.service" "$HOME/.config/systemd/user/gitla-log.service"
install -Dm644 "${archive_folder}/systemd/gitla-log.timer" "$HOME/.config/systemd/user/gitla-log.timer"

echo "Installation completed successfully."

# Temporary directory is automatically removed by trap
