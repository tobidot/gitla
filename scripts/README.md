# Scripts to rule them all

This idea actually comes from [github](https://github.com/github/scripts-to-rule-them-all).
Compared to `Makefile`s or package manager run scripts (e.g. `npm run ...`) this is a quite elegant/simple approach to having all relevant tasks documented in an executable way.
