# gitla

The GIT Log Aggregator is a command line tool that can fetch and aggregate the git logs of all git repositories below
the configured `Root`.
These logs are stored in a sqlite table which can be queried with full text search.

## Usage

### Example

Imagine you have several (i.e. >100) microservices/repositories you need to maintain.

One day one of the services exhibits a performance issue that you know you already solved once in one service.
But which was it?

Sadly your git management service `bitbucket` does not support searching for commit messages 🤦.
So you need to do it locally.
But crawling through all repos each time this happens becomes a chore, so you give up and simply start sampling git
repositories manually.

If this sounds familiar `gitla` has you covered.

```
❯ gitla search "fix AND sqlite AND (perf* OR speed)"
/home/elliot/Documents/allsafe/code/backup_crawler
└─ c089126910787345a21cb176fcad7565b2cb881d
└─ 2018-09-05T14:48:23+02:00
└─ elliot@allsafe.org
└─── fix: improve sqlite performance of crawler for vulnerable backups 


/home/elliot/Documents/allsafe/code/femto_cell
└─ 6f2bb0897ab8091d33a950277bebeca05633b63b
└─ 2017-11-19T10:44:34+00:00
└─ elliot@allsafe.org
└─── fix: speed up sqlite transactions for mapping network
```

### Details

- see [manpage](./docs/man.adoc)

## Installation

### Prerequisites

gitla assumes following executables to be present in the system:
 - `less`

### What does the installation script do?

- copy the manpage to `/.local/share/man/man1/gitla.1.gz` (you might want to reindex the mandb afterwards,
  see `./scripts/_dev/mandb-reindex`)
- compile `gitla` binary
- copy the binary to `~/.local/bin/gitla` (make sure this directory is part of your `$PATH`)
- copy the systemd timer for collecting logs hourly to `~/.config/systemd/user/` and enable and start it
- copy the completion script to `~/.bash_completions/gitla.sh` (make sure this directory is sourced from your `.bashrc`)

The scripts in `./scripts` are very simple, so it should be easy for you to adjust, customize this process to your
needs.
Simply pick and choose what parts of the installation you want to have installed on your machine.

### Installation via bash from gitlab release page

This uses the statically linked binary from the gitlab release page.

```bash
curl -O https://gitlab.com/api/v4/projects/42402355/packages/generic/gitla/0.3.0/install-gitla-on-linux-from-gitlab-release-binary_v0.3.0.sh
## review the shell script!
chmod +x install-gitla-on-linux-from-gitlab-release-binary_v0.3.0.sh
./install-gitla-on-linux-from-gitlab-release-binary_v0.3.0.sh
```

### Linux from local build

This builds the binary from the local source code (with dynamic linking) and the documentation before installing them.

```bash
./scripts/install/linux-from-local-build
```

The script also installs a systemd user service and timer (at `~/.config/systemd/user/`), to update the commits database at every full hour in the background.
By default the timer is not active.
To activate the timer run the following commands.

```bash
systemctl --user reenable gitla-log.timer
systemctl --user restart gitla-log.timer
```

Before activating the timer it makes sense to configure and run the the log collection manually a few times, to make sure you're actually collecting git logs from the relevant directories.

### First time Use

Before using `gitla` you need to create the default config file first via:

```shell
gitla createOrUpdateConfig
```

This will print out the path to the created config file for you.
Now you can customize the config to your liking.
Especially the `Root` directory for the search for git repositories is something you might want to adjust.

Once setup, you can aggregate the logs for the first time

```shell
gitla log
```

and then search through the log commit messages in the database

```shell
gitla search "vulnerability AND CVE"
```

You can furthermore use custom queries to quickly get the information you need.
Custom queries should be written to `~/.config/gitla/queries` and should contain valid sqlite queries.
The queries can be executed via

```shell
gitla query <query-file-name>
```

The query name is the filename of the query and is autocompleted.
Examples for useful queries can be found in this repository under `data/queries/`

For more details see the [man page](./docs/man.adoc).

## Roadmap

Prioritized list of next features (top-item = next one to be implemented).
They are just rough plans/reminders and hence may be subject to change (both in content and order).

### Backlog

- [ ] implement profiles (similar to aws-cli profiles)
- [ ] switch to [HuJSON](https://github.com/tailscale/hujson) for config parsing
- [ ] use slog for logging Debug logs
    - [] set log level via environment variable

